$(function(){
// Select all items in row
    $('.item-title').on('change', 'input[type="checkbox"]', function(event) {
        var checkBoxes = $(this).parents('tr').find('.ch-select input[type="checkbox"]').not($(this)),
            state = $(this).prop('checked');

        $(this).parents('tr').toggleClass('active-row');
        checkBoxes.prop('checked', state)
    });




// Change range type select variant
    var shangeRangeType = function(target){
        $('[data-show="'+target+'"]')
            .removeClass('hide')
            .siblings()
            .addClass('hide');   
    }


    shangeRangeType($('[data-show-target].active').data('show-target'))


    $('[data-show-target]').on('click', function(index, el) {
        var target = $(this).data('show-target');

        $('[data-show-target]')
            .removeClass('active');

        $(this).addClass('active');      

        shangeRangeType(target)
    });
});